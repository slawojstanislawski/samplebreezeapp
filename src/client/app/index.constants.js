/* global malarkey:false, toastr:false, moment:false */
(function () {
    'use strict';

    angular
        .module('app')
        .constant('moment', moment);
    //.constant('malarkey', malarkey)
    //.constant('toastr', toastr)
})();
