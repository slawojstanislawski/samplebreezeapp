(function () {
    'use strict';

    angular
        .module('app', [
            'ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ui.bootstrap',
            'app.core',
            'app.data',
            'app.layout',
            'app.dashboard',
            'app.user',
            'breeze.angular'
        ]);

})();
