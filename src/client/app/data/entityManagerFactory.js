angular.module('app.data')
    .factory('entityManagerFactory', ['breeze.config', emFactory]);

function emFactory(breezeConfig) {

    var breeze = breezeConfig.breeze;
    var remoteServiceName = breezeConfig.remoteServiceName;

    var factory = {
        newManager: function () {
            return new breeze.EntityManager(remoteServiceName);
        },
    };

    return factory;
}
