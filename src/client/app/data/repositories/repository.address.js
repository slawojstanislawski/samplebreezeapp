(function () {
    'use strict';

    angular
        .module('app.data')
        .factory('repository.address', RepositoryAddress);

    RepositoryAddress.$inject = ['breeze', 'model', 'repository.abstract'];

    function RepositoryAddress(breeze, model, AbstractRepository) {
        var addressesQuery = breeze.EntityQuery.from('Addresses');
        var entityName = model.entityNames.address;
        var orderBy = 'street';

        return {
            create: createRepo // factory function to create the repository
        };

        /* Implementation */
        function createRepo(manager) {
            var base = new AbstractRepository(manager, entityName);
            var count;
            var repo = {
                getAll: getAll,
                getCount: getCount,
                getFilteredCount: getFilteredCount
            };

            return repo;

            function streetNamePredicate(filterValue) {
                return breeze.Predicate
                    .create('street', 'contains', filterValue)
                    //.or('postal', 'contains', filterValue);
            }

            function getAll(forceRemote, page, size, nameFilter) {

                if (base.zStorage.areItemsLoaded('addresses') && !forceRemote) {
                    // Get the addresses from local cache
                    return base.$q.when(base.getAllLocal(entityName, orderBy));
                }

                // Load all the addresses to cache via remote query
                return addressesQuery
                    .orderBy(orderBy)
                    .toType(entityName)
                    .using(base.manager).execute()
                    .then(querySucceeded).catch(base.queryFailed);

                function querySucceeded(data) {
                    //var addresses = base.setIsPartialTrue(data.results); //I get whole addresses, not projections, left here just like this...
                    var addresses = data.results;
                    base.zStorage.areItemsLoaded('addresses', true);
                    console.log("areAddressesLoaded", base.zStorage.areItemsLoaded('addresses'));
                    base.logger.info('Retrieved [Addresses] from remote data source', addresses.length);
                    base.zStorage.save();
                    return addresses;
                }
            }

            function getCount() {
                if (base.zStorage.areItemsLoaded('addresses')) {
                    return base.$q.when(base.getLocalEntityCount(entityName));
                }
                if (count !== undefined) {
                    return base.$q.when(count);
                }
                // Addresses aren't loaded and don't have a count yet;
                // ask the server for a count and remember it
                return addressesQuery.take(0).inlineCount()
                    .using(base.manager).execute()
                    .then(function (data) {
                        count = data.inlineCount;
                        return count;
                    });
            }

            function getFilteredCount(nameFilter) {
                var predicate = breeze.Predicate
                    .and(base.predicates.isNotNullo)
                    .and(streetNamePredicate(nameFilter));

                var addresses = addressesQuery
                    .where(predicate)
                    .using(base.manager)
                    .executeLocally();

                return addresses.length;
            }

        }
    }
})();