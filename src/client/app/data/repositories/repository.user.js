(function () {
    'use strict';

    angular
        .module('app.data')
        .factory('repository.user', RepositoryAttendee);

    RepositoryAttendee.$inject = ['breeze', 'model', 'repository.abstract'];

    function RepositoryAttendee(breeze, model, AbstractRepository) {
        var usersQuery = breeze.EntityQuery.from('Users');
        var entityName = model.entityNames.user;
        var orderBy = 'lastName, firstName';

        return {
            create: createRepo // factory function to create the repository
        };

        /* Implementation */
        function createRepo(manager) {
            var base = new AbstractRepository(manager, entityName);
            var count;
            var repo = {
                create: create,
                getAll: getAll,
                getById: base.getById,
                getCount: getCount,
                getFilteredCount: getFilteredCount
            };

            return repo;

            function create() {
                return manager.createEntity(entityName);
            }

            function fullNamePredicate(filterValue) {
                return breeze.Predicate
                    .create('firstName', 'contains', filterValue)
                    .or('lastName', 'contains', filterValue);
            }

            function getAll(forceRemote, page, size, nameFilter) {
                // Only return a page worth of users
                var take = size || 20;
                var skip = page ? (page - 1) * size : 0;

                if (base.zStorage.areItemsLoaded('users') && !forceRemote) {
                    // Get the page of users from local cache
                    return base.$q.when(getByPage());
                }

                // Load all users to cache via remote query
                return usersQuery
                    .select('id, firstName, lastName, addressId, jobId')
                    //.expand('job')//select and expand are not supported together in a single query.
                    .orderBy(orderBy)
                    .toType(entityName)
                    .using(base.manager).execute()
                    .then(querySucceeded).catch(base.queryFailed);

                function querySucceeded(data) {
                    var users = base.setIsPartialTrue(data.results);
                    base.zStorage.areItemsLoaded('users', true);
                    console.log("areUsersLoaded", base.zStorage.areItemsLoaded('users'));
                    base.logger.info('Retrieved [Users] from remote data source', users.length);
                    base.zStorage.save();
                    return getByPage();
                }

                function getByPage() {
                    var predicate = base.predicates.isNotNullo;

                    if (nameFilter) {
                        predicate = predicate.and(fullNamePredicate(nameFilter));
                    }

                    var users = usersQuery
                        .where(predicate)
                        .orderBy(orderBy)
                        .take(take).skip(skip)
                        .using(base.manager)
                        .executeLocally();

                    return users;
                }

            }

            function getCount() {
                if (base.zStorage.areItemsLoaded('users')) {
                    return base.$q.when(base.getLocalEntityCount(entityName));
                }
                if (count !== undefined) {
                    return base.$q.when(count);
                }
                // Users aren't loaded and don't have a count yet;
                // ask the server for a count and remember it
                return usersQuery.take(0).inlineCount()
                    .using(base.manager).execute()
                    .then(function (data) {
                        count = data.inlineCount;
                        return count;
                    });
            }

            function getFilteredCount(nameFilter) {
                var predicate = breeze.Predicate
                    .and(base.predicates.isNotNullo)
                    .and(fullNamePredicate(nameFilter));

                var users = usersQuery
                    .where(predicate)
                    .using(base.manager)
                    .executeLocally();

                return users.length;
            }

        }
    }
})();