(function () {
    'use strict';

    angular
        .module('app.data')
        .factory('repository.job', RepositoryJob);

    RepositoryJob.$inject = ['breeze', 'model', 'repository.abstract'];

    function RepositoryJob(breeze, model, AbstractRepository) {
        var jobsQuery = breeze.EntityQuery.from('Jobs');
        var entityName = model.entityNames.job;
        var orderBy = 'title';

        return {
            create: createRepo // factory function to create the repository
        };

        /* Implementation */
        function createRepo(manager) {
            var base = new AbstractRepository(manager, entityName);
            var count;
            var repo = {
                getAll: getAll,
                getCount: getCount,
                getFilteredCount: getFilteredCount
            };

            return repo;

            function jobTitlePredicate(filterValue) {
                return breeze.Predicate
                    .create('title', 'contains', filterValue)
                    //.or('lastName', 'contains', filterValue);
            }

            function getAll(forceRemote, page, size, nameFilter) { //IT would be named getPartials in the original application, TODO: decide on the naming.

                if (base.zStorage.areItemsLoaded('jobs') && !forceRemote) {
                    // Get the jobs from local cache
                    return base.$q.when(base.getAllLocal(entityName, orderBy));
                }

                // Load all users to cache via remote query
                return jobsQuery
                    .orderBy(orderBy)
                    .toType(entityName)
                    .using(base.manager).execute()
                    .then(querySucceeded).catch(base.queryFailed);

                function querySucceeded(data) {
                    //var jobs = base.setIsPartialTrue(data.results);//I get whole jobs, not projections, left here just like this...
                    var jobs = data.results;
                    base.zStorage.areItemsLoaded('jobs', true);
                    console.log("areJobsLoaded", base.zStorage.areItemsLoaded('jobs'));
                    base.logger.info('Retrieved [Jobs] from remote data source', jobs.length);
                    base.zStorage.save();
                    return jobs;
                }
            }

            function getCount() {
                if (base.zStorage.areItemsLoaded('jobs')) {
                    return base.$q.when(base.getLocalEntityCount(entityName));
                }
                if (count !== undefined) {
                    return base.$q.when(count);
                }
                // Jobs aren't loaded and don't have a count yet;
                // ask the server for a count and remember it
                return jobsQuery.take(0).inlineCount()
                    .using(base.manager).execute()
                    .then(function (data) {
                        count = data.inlineCount;
                        return count;
                    });
            }

            function getFilteredCount(nameFilter) {
                var predicate = breeze.Predicate
                    .and(base.predicates.isNotNullo)
                    .and(jobTitlePredicate(nameFilter));

                var jobs = jobsQuery
                    .where(predicate)
                    .using(base.manager)
                    .executeLocally();

                return jobs.length;
            }

        }
    }
})();