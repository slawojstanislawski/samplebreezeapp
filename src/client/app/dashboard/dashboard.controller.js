(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('DashboardController', ['$timeout', 'datacontext', 'common', DashboardController]);

    /** @ngInject */
    function DashboardController($timeout, datacontext, common) {
        var vm = this;

        vm.awesomeThings = [];
        vm.classAnimation = '';
        vm.creationDate = 1439302051929;

    }
})();
