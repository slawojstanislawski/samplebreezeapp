(function () {
    'use strict';

    angular
        .module('app.user')
        .controller('UserDetailsController', ['$q', '$scope', '$stateParams', '$timeout', '$window', '$location', 'bootstrap.dialog', 'config', 'datacontext', 'common', 'logger', UserDetailsController]);

    /** @ngInject */
    function UserDetailsController($q, $scope, $stateParams, $timeout, $window, $location, bsDialog, config, datacontext, common, logger) {
        var vm = this;
        var wipEntityKey;

        vm.datepickerMaxDate = new Date();
        vm.cancel = cancel;
        vm.deleteUser = deleteUser;
        vm.goBack = goBack;
        vm.hasChanges = false;
        vm.isSaving = false;
        vm.save = save;
        vm.user = undefined;
        Object.defineProperty(vm, 'canSave', {get: canSave});

        activate();

        function activate() {
            //initLookups();
            onDestroy();
            onHasChanges();
            return getRequestedUser();
        }

        function onDestroy() {
            $scope.$on('$destroy', function () {
                //autoStoreWip(true);
                datacontext.cancel();
            });
        }

        function onHasChanges() {
            $scope.$on(config.events.hasChangesChanged,
                function(event, data) {
                    vm.hasChanges = data.hasChanges;
                });
        }

        function getRequestedUser() {
            var val = $stateParams.userId;

            if (val === 'new') {
                vm.user = datacontext.user.create();
                return vm.user;
            }

            return datacontext.user.getById(val)
                .then(function (data) {
                    if (data) {
                        // data is either an entity or an {entity, wipKey} pair
                        wipEntityKey = data.key;
                        console.log("wipEntityKey", wipEntityKey);
                        //vm.session = data.entity || data;
                        vm.user = data;
                    } else {
                        logger.warning('Could not find user id = ' + val);
                        goToUsers();
                    }
                })
                .catch(function (error) {
                    logger.error('Error while getting user id = ' + val + '; ' + error);
                    goToUsers();
                });
        }

        function goToUsers() {
            $location.path('/users');
        }

        function goBack() {
            $window.history.back();
        }

        function canSave() {
            return vm.hasChanges && !vm.isSaving;
        }

        function save() {
            if (!canSave()) {
                return $q.when(null);
            } // Must return a promise

            vm.isSaving = true;
            return datacontext.save().then(function(saveResult) {
                vm.isSaving = false;
                //removeWipEntity();
                //common.replaceLocationUrlGuidWithId(vm.session.id);
                //datacontext.speaker.calcIsSpeaker();
            }).catch(function(error) {
                vm.isSaving = false;
            });
        }

        function cancel() {
            datacontext.cancel(); //TODO: think on it again, all data changes pending, are cancelled. Perhaps it's a sane behavior.
            //removeWipEntity();
            //common.replaceLocationUrlGuidWithId(vm.session.id);
            if (vm.user.entityAspect.entityState.isDetached()) {
                goToUsers();
            }
        }

        function deleteUser() {
            return bsDialog.deleteDialog('User')
                .then(confirmDelete);

            function confirmDelete() {
                datacontext.markDeleted(vm.user);
                vm.save().then(success).catch(failed);

                function success() {
                    //removeWipEntity();
                    goToUsers();
                }

                function failed(error) {
                    logger.error("Error while removing User", "Remove error", error);
                    cancel(); // Makes the entity available to edit again
                }
            }
        }

    }
})();
