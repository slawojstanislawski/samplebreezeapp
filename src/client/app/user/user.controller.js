(function () {
    'use strict';

    angular
        .module('app.user')
        .controller('UserController', ['datacontext', '$location', '$q', UserController]);

    /** @ngInject */
    function UserController(datacontext, $location, $q) {
        var vm = this;
        vm.goToUser = goToUser;
        vm.refresh = refresh;

        vm.awesomeThings = [];
        vm.classAnimation = '';
        vm.creationDate = 1439302051929;

        activate();

        function activate() {
            return getUsers().then(function (users) {
                //getUsersCount();
                vm.usersCount = users.length;
                //applyFilter = common.createSearchThrottle(vm, 'sessions');
                //if (vm.sessionsSearch) {
                //    applyFilter(true /*now*/);
                //}
            });
        }

        function refresh() {
            getUsers(true);
        }

        //note that getUsers has getAll() call, and it returns just partials.
        function getUsers(forceRemote) {
            return datacontext.user.getAll(forceRemote).then(function handleUsers(users) {
                vm.users = users;
                return users;
            });
        }

        function getUsersCount() {
            datacontext.user.getCount().then(function setUsersCount(count) {
                vm.usersCount = count;
            });
        }

        function goToUser(user) {
            if (user && user.id) {
                $location.path('/users/' + user.id);
            }
        }

    }
})();
