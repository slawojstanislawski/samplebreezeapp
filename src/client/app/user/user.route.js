(function() {
    'use strict';

    angular
        .module('app.user')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'users',
                config: {
                    url: '/users',
                    templateUrl: 'app/user/user.html',
                    controller: 'UserController',
                    controllerAs: 'vm',
                    title: 'Users',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-dashboard"></i> Users'
                    }
                }
            },
            {
                state: 'userdetails',
                config: {
                    url: '/users/:userId',
                    templateUrl: 'app/user/userdetails.html',
                    controller: 'UserDetailsController',
                    controllerAs: 'vm',
                    title: 'User Details',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-dashboard"></i> Users'
                    }
                }
            }
        ];
    }
})();
