(function () {
    'use strict';

    angular
        .module('app')
        .run(runBlock);

    /** @ngInject */
    function runBlock($log, $rootScope, $timeout) {
        $log.debug('runBlock end');
        $rootScope.$on('$stateChangeStart', function (event, next, current) {
            $rootScope.showSplash = true;
        });
        $rootScope.$on('$stateChangeSuccess', function (event, next, current) {
            $timeout(function() {
                $rootScope.showSplash = false;
            }, 300);
        });
        $rootScope.$on('$stateChangeError', function (event, next, current) {
            $rootScope.showSplash = false;
        });
    }

})();
